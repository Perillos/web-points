/**
 * Cada alumno será un objeto con las siguientes propiedades.
 * name: string
 * Puntos: number
 * Historial De Puntos: array de objetos. [ {date: formato unix, points} ];
 */

/**
 * Para que no se me olvide:
 * Un punto para todos, excepto Raquel, Mathias y Javi
 * 
 * Un punto para Fernando
 * 
 * Un punto para Gloria
 * 
 * Un punto para Mireia
 * 
 * Un punto para Luis
 */


/**
 * Puntos extras 17/05
 * Mireia, Fernando y Luis por colaborar y trabajar juntos en discord
 */
const alumns = [
    {
        name: 'Alberto',
        lastName: 'Núñez',
        points: 0,
        history: [],
    },
    {
        name: 'Alejandro',
        lastName: 'Rodríguez',
        points: 0,
        history: [],
    },
    {
        name: 'Carlos',
        lastName: 'De La Cruz',
        points: 0,
        history: [],
    },
    {
        name: 'Fernando',
        lastName: 'Valero',
        points: 0,
        history: [],
    },
    {
        name: 'Fran',
        lastName: 'Riquelme',
        points: 0,
        history: [],
    },
    {
        name: 'Gloria',
        lastName: 'Vega',
        points: 0,
        history: [],
    },
    {
        name: 'Luis',
        lastName: 'Camino',
        points: 0,
        history: [],
    },
    {
        name: 'Mar',
        lastName: 'Andrés',
        points: 0,
        history: [],
    },
    {
        name: 'Mathias',
        lastName: 'Barros',
        points: 0,
        history: [],
    },
    {
        name: 'Mireia',
        lastName: 'García',
        points: 0,
        history: [],
    },
    {
        name: 'Javier',
        lastName: 'Amián',
        points: 0,
        history: [],
    },
    {
        name: 'Pau',
        lastName: 'Isach',
        points: 0,
        history: [],
    },
    {
        name: 'Raquel',
        lastName: 'Deligeorges',
        points: 0,
        history: [],
    },
    {
        name: 'Paula',
        lastName: 'Hernández',
        points: 0,
        history: [],
    },
    {
        name: 'Victor',
        lastName: 'León',
        points: 0,
        history: [],
    },
];

const createAlumnPoints = (alumn) => {
    const item = document.createElement('div');
    item.classList.add('item');

    const name = document.createElement('div');
    name.classList.add('item__name');
    name.innerText = `Nombre ${alumn.name} ${alumn.lastName}`;

    const points = document.createElement('div');
    points.classList.add('item__points');
    points.innerText = `Puntos ${alumn.points}`;

    item.appendChild(name);
    item.appendChild(points);

    const list = document.querySelector('.list');
    list.appendChild(item);
};

alumns.forEach((alumn) => {
    createAlumnPoints(alumn);
});